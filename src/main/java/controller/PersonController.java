package controller;

import java.util.List;

import javax.annotation.Resource;

import model.*;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import view.PersonForm;
import dao.PersonDao;

@Controller
public class PersonController {

    @Resource
    private PersonDao personDao;

    @RequestMapping("/")
    public String personList(ModelMap model) {
    	
    	model.addAttribute("persons", personDao.getAllPersons());
        return "list";
    }

    @RequestMapping("/add")
    public String showForm(@ModelAttribute("personForm") PersonForm form) {
        return "form";
    }

    @RequestMapping("/view/{personId}")
    public String view(
            @ModelAttribute("personForm") PersonForm form,
            @PathVariable Long personId) {

    	form.setPerson(personDao.getPersonById(personId));
    	form.setDisabled(true);
    	
        return "form";
    }
    
    @RequestMapping("/edit/{personId}")
    public String edit(
    		@ModelAttribute("personForm") PersonForm form,
    		@PathVariable Long personId) {
    	
    	form.setPerson(personDao.getPersonById(personId));
    	form.setDisabled(false);
    	
    	return "form";
    }

    @RequestMapping("/save")
    public String saveForm(
            @ModelAttribute("personForm") PersonForm form) {

    	Person person = form.getPerson();
    	
    	if (form.getAddPhoneButton() != null) {
    		person.addPhone(new Phone());
    		return "form";
		}else if(person.getPhoneWithDeletePressed() != null){
			person.removePhone(person.getPhoneWithDeletePressed());
			return "form";
		}else{
			personDao.save(person);
			return "redirect:/";
		}
    }
}