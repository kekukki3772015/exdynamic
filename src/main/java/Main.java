import model.Person;
import model.Phone;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import conf.DataConfig;
import dao.PersonDao;

public class Main {

    public static void main(String[] args) {

        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(DataConfig.class);

        PersonDao dao = ctx.getBean(PersonDao.class);

        Person person = new Person("Madis");
        person.addPhone(new Phone("123"));
        person.addPhone(new Phone("456"));
        
        dao.save(person);
        
        Person loaded = dao.getPersonById(1l);
        
        loaded.addPhone(new Phone("789"));
        loaded.getPhones().remove(0);
        
        dao.save(loaded);
        
        System.out.println(dao.getAllPersons());

        ((ConfigurableApplicationContext) ctx).close();
    }

}
