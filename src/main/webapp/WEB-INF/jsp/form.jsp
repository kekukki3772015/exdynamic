<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="styles.jsp"%>
</head>
<body>
	<%@ include file="menu.jsp"%>

	<c:url value="/save" var="theAction" />
	<form:form method="post" action="${theAction}"
		modelAttribute="personForm">

	<form:input type="hidden" path="person.id"/>

    Eesnimi: <form:input disabled="${personForm.disabled}"
			path="person.name" />
		<br />
		<br />
		<br />

    Telefonid:
    <br />
		<br />

		<c:forEach var="phone" items="${personForm.person.phones}"
			varStatus="status">
			<form:input disabled="${personForm.disabled}"
				path="person.phones[${status.index}].number" />

			<c:if test="${not personForm.disabled}">
				<form:input path="person.phones[${status.index}].deleteButton"
					type="submit" value="kustuta" />
			</c:if>
			<br />
		</c:forEach>

		<c:if test="${not personForm.disabled}">
			<form:input path="addPhoneButton" type="submit" value="Lisa telefon" />
			<form:input path="saveButton" type="submit" value="Salvesta" />
		</c:if>


	</form:form>
</body>
</html>